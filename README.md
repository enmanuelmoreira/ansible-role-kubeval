# Ansible Role: Kubeval

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-kubeval/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-kubeval/-/commits/main)


This role installs [Kubeval](https://kubeval.instrumenta.dev/) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    kubeval_version: latest # tag "0.16.0" if you want a specific version
    kubeval_arch: amd64 # amd64 or 386
    setup_dir: /tmp
    kubeval_bin_path: /usr/local/bin/kubeval
    kubeval_repo_path: https://github.com/instrumenta/kubeval/releases/download

This role can install the latest or a specific version. See [available Kubeval releases](https://github.com/instrumenta/kubeval/releases/) and change this variable accordingly.

    kubeval_version: latest # tag "0.16.0" if you want a specific version

The path of the Kubeval repository.

    kubeval_repo_path: https://github.com/instrumenta/kubeval/download

The path to the home Kubeval directory.

    kubeval_bin_path: /usr/local/bin/kubeval

The location where the Kubeval binary will be installed.

The path to the home Kubeval directory.

    kubeval_arch: amd64 # amd64 or 386

Kubeval supports 386 and amd64 CPU architectures, just change for the main architecture of your CPU.

Kubeval needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install Kubeval. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: kubeval

## License

MIT / BSD
